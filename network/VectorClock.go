package network

import (
	"encoding/gob"
	"math"
)

type VectorClock struct {
	Clock map[string]int
}

func NewVectorClock() *VectorClock {
	gob.Register(map[string]int{})
	return &VectorClock{Clock: make(map[string]int)}
}

func (v *VectorClock) UpdateClockBeforeSend(packet *Packet) {
	v.Clock[GetSelfIPPortAddress()] += 1
	packet.PClock = v.Clock
}

func (v *VectorClock) UpdateClockOnReceived(packet *Packet) {
	v.Clock[GetSelfIPPortAddress()] += 1
	v.Clock = mergeClocks(v.Clock, packet.PClock.(*VectorClock).Clock)
	packet.PClock = v
}

func (v *VectorClock) inferiorOrEqual(clock IClock) bool {
	var vClock = clock.(*VectorClock)

	vi := v.Clock
	for j, vj := range vClock.Clock {
		if !(vi[j] <= vj){
			return false
		}
	}

	return true
}

func (v *VectorClock) strictlyInferior(clock IClock) bool {
	var vClock = clock.(*VectorClock)

	var isIE = v.inferiorOrEqual(vClock)

	vi := v.Clock
	for j, vj := range vClock.Clock {
		if vi[j] != vj{
			return isIE
		}
	}

	return false
}

func (v *VectorClock) CompareTo(clock IClock) bool {
	var vClock = clock.(*VectorClock)

	/*
	   Vi <= Vj si et seulement si ∀k,Vi[k]≤Vj[k]
	   Vi <  Vj si et seulement si Vi≤Vj et Vi != Vj
	*/


	// attendre que ∀j,Vi[j] + 1≥V[j];
	var vi = VectorClock{Clock: v.Clock}
	for j := range vi.Clock {
		vi.Clock[j]++
	}

	return vClock.strictlyInferior(&vi)
}

func mergeClocks(v1 map[string]int, v2 map[string]int) map[string]int {
	set := make(map[string]int)
	for key, value := range v1 {
		set[key] = value
	}
	for key, value := range v2 {
		set[key] = value
	}
	for key := range set {
		v1Value := v1[key]
		v2Value := v2[key]
		set[key]= int(math.Max(float64(v1Value), float64(v2Value)))
	}
	return set
}

