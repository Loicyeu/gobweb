package network

func cleanInsert(slice []*Packet, elems ...*Packet) []*Packet {
	for i, v := range slice {
		if v == nil && len(elems) > 0 {
			var x *Packet
			x, elems = pop(elems)
			slice[i] = x
		}
	}

	if len(elems) != 0 {
		slice = append(slice, elems...)
	}
	return slice
}


func pop(slice []*Packet) (*Packet, []*Packet) {
	if len(slice)>1 {
		return slice[len(slice)-1], slice[:len(slice)-1]
	}else{
		return slice[0], []*Packet{}
	}
}

