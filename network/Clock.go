package network

type IClock interface {
	UpdateClockBeforeSend(packet *Packet)
	UpdateClockOnReceived(packet *Packet)
	CompareTo(clock IClock) bool
}
